<!-- Se genera el documento html -->
<!DOCTYPE html>
<html>

<!-- se crea el encabezado -->

<head>
	<!-- formato utf para caracteres extraños -->
	<meta charset="utf-8">
	<!-- se crea el titulo de la pagina -->
	<title> Página Ejercicio 2 </title>
	<!-- se llama al archivo que contiene los estilos -->
	<link rel="stylesheet" type="text/css" href="../estilos.css">
	<!-- fuente de google para el titulo -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet"> 
</head>

<!-- cuerpo de la pagina -->

<body>
	<!-- titulo y parrafo de informacion -->
	<h1> Ejercicio 2 </h1>
	<h2> TABLA 2. Filas y columnas N </h2>
	<p> Esta tabla tiene dimensiones NxN, lo que significa que el valor determinado por el creador sera igual al número de filas y columnas que se generen, mostrando en la tabla todos los números en sucesión. En este caso el valor definido en el tamaño es igual a 15 </p>
	<!-- se comienza a ejecutar el archivo php -->
	<?php
	// Se define el tamaño de las filas y columnas en este caso es 15
	define('TAM', 15);
	// De esta manera se crea la tabla
	echo "<table border=2 align=center>";
	// este es el contador
	$n = 1;
	// se crea el ciclo for que crea las filas
	for ($n1 = 1; $n1 <= TAM; $n1++) {
		// se crea este if que permite que se coloreen de manera intermedia las filas
		if ($n1 % 2 == 0) {
			// si es par la fila sera de color gris
			echo "<tr bgcolor = #aec3b7 >";
		} else {
			// si no se cumple la condicion sera de color blanco
			echo "<tr bgcolor = #fcfffd >";
		}

		// se crea el ciclo for que crea las columnas
		for ($n2 = 1; $n2 <= TAM; $n2++) {
			// se crea cada una de las celdas
			echo "<td>", $n, "</td>";
			// se crea la suma para que aumente el numero de celdas
			$n = $n + 1;
		}
		echo "</tr>";
	}
	// se finaliza la tabla
	echo "</table>";
	?>
</body>

</html>