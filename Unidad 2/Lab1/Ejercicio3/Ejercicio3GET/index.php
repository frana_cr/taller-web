<!-- Se inicia el archivo html-->
<!DOCTYPE html>
<html lang="en">
<!-- Encabezado de la pagina creada -->

<head>
  <!-- formato utf para caracteres extraños -->
  <meta charset="utf-8">
  <!-- se crea el titulo de la pagina -->
  <title> Página Ejercicio 3GET </title>
  <!-- se llama al archivo que contiene los estilos -->
  <link rel="stylesheet" type="text/css" href="../../estilos.css">
  <!-- fuente de google para el titulo -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
</head>

<!-- cuerpo de la pagina -->

<body>
  <!-- Etiqueta principal del ejercicio -->
  <h1> Ejercicio 3B </h1>
  <!-- Parrafo con la informacion de que trata -->
  <p> A continuación se muestra como a partir de una variable N (el número que ingrese el usuario)
    se generará una tabla de dimensiones NxN utilizando el método de GET.
    También se colorean de manera intermedias las filas segun el color que el usuario determine.
    Luego de ingresar las opciones el programa de la página es reiniciado.
    Al final se muestra el resultado y las opciones ingresadas. </p>
  <!-- con esta marquesa se movera el texto de ayuda al usuario -->
  <marquee bgcolor=#56f800 width="500px" height="50px">
    <!-- impresion de la informacion -->
    <h4>SELECCIONA LOS COLORES E INGRESA UN VALOR PARA LA TABLA</h4>
  </marquee>
  <!-- Se crea el formulario y el metodo que se ocupa en este caso GET -->
  <form method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
    <!-- Se da inicio a la tabla en donde el usuario ingresara sus opciones -->
    <table>
      <tr>
        <!-- De esta manera se imprime lo que se muestra al usuario -->
        <td align="left"> Número de filas y columnas:</TD>
        <!-- Con esta opcion se guarda la variable -->
        <td align="right" colspan="3"><input type="text" name="variable" size="21"></td>
      </tr>
      <tr>
        <!-- se imprime cual es el color de la tabla de las filas pares mediante el uso de input type color -->
        <p> Color filas pares: <input type="color" name="colorfilaspares"></p>
      </tr>
      <tr>
        <!-- se imprime cual es el color de la tabla de las filas impares -->
        <p> Color filas impares: <input type="color" name="colorfilasimpares"></p>
      </tr>
    </table><br>
    <!-- existira la opcione que permite enviar las opciones y confirmar -->
    <input type="submit" value="Enviar opciones"></p>
  </form>
  <!-- luego del salto de linea se mostrara la tabla y los detalles de opciones -->
  <hr>
  </center>

  <!-- Se da comienzo a la zona en donde se utiliza el lenguaje PHP para poder generar la tabla y mostrar la informacion -->
  <?php
  // se muestra cuales seran las filas y tambien se le otorga otro nombre a la variable
  if (isset($_GET["variable"])) {
    // definicion del nuevo nombre variable
    $numero_variable = $_GET["variable"];
    // se imprime cual sera el numero de filas y columnas
    echo "<p> El número de filas y columnas es: " . $numero_variable . "</p>";
  }
  // se muestra la informacion y el nuevo nombre del color de las filas pares de la pagina
  if (isset($_GET["colorfilaspares"])) {
    // definicion nuevo nombre del color
    $color1 = $_GET["colorfilaspares"];
    // se muestra cual sera el color elegido
    echo "<p> El color filas pares es: " . $color1 . "</p>";
  }

  // se muestra la informacion y el nuevo nombre del color de las filas imparesde la pagina
  if (isset($_GET["colorfilasimpares"])) {
    // definicion nuevo nombre del color
    $color2 = $_GET["colorfilasimpares"];
    // se muestra cual sera el color elegido
    echo "<p> El color filas impares es: " . $color2 . "</p>";
  }

  // se da comienzo a la generacion de la tabla 
  echo "<table border=2 align=center>";
  // el contador inicia en 1
  $n = 1;
  // se crea el ciclo for que genera las filas
  for ($n1 = 1; $n1 <= $numero_variable; $n1++) {
    // cuando la fila sea par esta sera coloreada (fila x medio)
    if ($n1 % 2 == 0) {
      // tendra la coloracion del color estimado por el usuario
      echo "<tr bgcolor = $color1>";
    }
    // de manera contraria sera de color natural (sin tono)
    else {
      echo "<tr bgcolor = $color2>";
    }
    // se crea el ciclo for que permite la creacion de las columnas
    for ($n2 = 1; $n2 <= $numero_variable; $n2++) {
      // se crea cada una de las celdas
      echo "<td>", $n, "</td>";
      // se crea la suma para que aumente el numero de celdas
      $n = $n + 1;
    }
    echo "</tr>";
  }
  // se finaliza la tabla
  echo "</table>";
  ?>
</body>

</html>
