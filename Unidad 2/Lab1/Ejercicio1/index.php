<!-- Se genera el documento html -->
<!DOCTYPE html>
<html>

<!-- Encabezado de pagina -->

<head>
	<!-- formato utf para caracteres extraños -->
	<meta charset="utf-8">
	<!-- se crea el titulo de la pagina -->
	<title> Página Ejercicio 1 </title>
	<!-- se llama al archivo que contiene los estilos -->
	<link rel="stylesheet" type="text/css" href="../estilos.css">
	<!-- fuente de google para el titulo -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet"> 
</head>

<!-- cuerpo de la pagina -->

<body>
	<!-- De esta manera se crea la etiqueta principal, una segunda especificando y el parrafo -->
	<h1> Ejercicio 1 <h1>
			<h2> TABLA 1. 10 Filas y 10 columnas </h2>
			<p> Esta tabla tiene dimensiones 10x10, a su vez se muestran los números del 1 al 100. </p>
			<!-- se comienza a ejecutar el archivo php -->
			<?php
			// Se definen los tamaños de las filas y columnas
			$columnas = 10;
			$filas = 10;
			// se crea la tabla 
			echo "<table border=2 align=center>";
			// este es el contador
			$n = 1;
			// se crea un ciclo for que crea las filas
			for ($n1 = 1; $n1 <= $columnas; $n1++) {
				// se imprimen las columnas 
				echo "<tr>";
				// se crea el ciclo for que crea las columnas
				for ($n2 = 1; $n2 <= $filas; $n2++) {
					// se crea la impresion de aquellas que sean las columnas
					echo "<td>", $n, "</td>";
					// se genera una suma que aumente el nivel del contador para tener nuevas celdas
					$n = $n + 1;
				}
				// terminan las columnas
				echo "</tr>";
			}
			// se finaliza la tabla
			echo "</table>";
			?>

</body>

</html>