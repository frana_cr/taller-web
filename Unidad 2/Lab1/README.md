# Laboratorio 1 Unidad 2.
## Explicacion general.
En el siguiente laboratorio se desarrolla la construcción de distintas paginas web en las que se muestra una serie de implementaciones de tablas con distitnas utilidades.
Se utilizan imagenes, tablas junto con distintas herramientas de PHP, HTML y CSS.

## Desarrollo.
Para el correcto funcionamiento del sitio que se ha creado se generaron 5 archivos PHP (llamados index.php) los cuales se almacenan dentro de distintas carpetas con su nombre y 1 CSS el encargando del estilo de todas ellas. 
Los archivos de cada ejercicio y sus funcionalidades son:

    -Ejercicio1: Se muestra en pantalla una tabla de 10 filas y 10 columnas con los n ́umeros del 1 al 100 (10x10) en cada celda. Para esto se define que el numero de las columnas y las filas sera igual a 10. 

    -Ejercicio2: En pantalla se muestra una tabla de N filas y N columnas con los n ́umeros del 1 al NxN. Tambien se colorean las filas alternando gris (gris pares) y blanco (impares). El tamano sera una constante: define(TAM, N). El valor de N es igual a 15.

    -Ejercicio3: En base al ejercicio anterior se tiene que modificar en este caso en pantalla se mostraran las opcioens para que el usuario ingrese el valor de N de una tabla NxN y tambien del color intermedio de las filas, luego de confirmar lo que desea envia y se visualiza el resultado y las opciones preestablecidas. Se utiliza los metodos por separado de GET y POST

        -Ejercicio3POST: Este ejercicio se termina y completa utilizando el metodo post.
        -Ejercicio3GET:  Este ejercicio se termina y completa utilizando el metodo get.
        
    -Ejercicio4: Se hace un programa que muestre en una tabla de 4 columnas en las cuales se veran todas las fotos almacenadas en una carpeta cuando se llega al numero 4 de las fotografias se generara una nueva fila. Se realizo mediante la ayuda de las funciones de directorio opendir, readdir, closedir)


## Prerequisitos
- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (Visual Studio Code)


## Ejecutando Pruebas
Para ejecutar el desarrollo del laboratorio1 se tiene que acceder mediante el local host del usuario y desde su carpeta public_html en donde luego se ingresa a la carpeta llamada taller web, posteriormente se accede a unidad 2 y lab 1. Se desplegara la serie de los ejercicios y se tienen que ejecutar seleccionando la que sea de nombre index.php o directamente en el numero de la carpeta deseada.
Ruta donde estan los archivos.
- http://localhost/~francisca/taller-web/Unidad%202/Lab1/

  #### Construido con:
    - Ubuntu: Sistema operativo.
    - HTML: Lenguaje de programación.
    - PHP: Lenguaje de programación.
    - Visual Studio Code: Editor de código.

## Creación
Francisca Castillo (Desarrollo de código y edición README)
