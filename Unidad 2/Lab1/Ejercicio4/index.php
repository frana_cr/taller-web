<!-- Se genera el documento html -->
<!DOCTYPE html>
<html>
<!-- se crea el encabezado -->

<head>
        <!-- formato utf para caracteres extraños -->
        <meta charset="utf-8">
        <!-- se crea el titulo de la pagina -->
        <title> Página Ejercicio 4 </title>
        <!-- se llama al archivo que contiene los estilos -->
        <link rel="stylesheet" type="text/css" href="../estilos.css">
        <!-- fuente de google para el titulo -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet"> 
</head>

<!-- cuerpo de la pagina -->

<body>
        <!-- titulo y parrafo de informacion -->
        <h1> Ejercicio 4 </h1>
        <!-- etiqueta secundaria de la pagina -->
        <h2> TABLA 4. Fotografías</h2>
        <!-- parrafo con informacion -->
        <p> Ahora se puede visualizar mediante una tabla distintas fotos que estan en una carpeta específica
                Se mostrarán sólo 4 fotos por cada una de las filas, para la realización se ocuparon algunas herramientas
                de directorio de PHP. <br>
                A contunación podrás conocer las fotografías.</p>

        <!-- se comienza a ejecutar el archivo php -->
        <?php
        // se procede a generar la tabla
        echo "<table border=2 align=center>";
        // se crean las columnas, sobre todas estara un encabezado.
        echo "<tr>";
        echo "  <th colspan=200> Figuras por columna  </th>";
        echo "</tr>";
        // se procede a llamar al directorio donde estaran las imagenes
        $directorio = "imagenes/";
        // Se abre el directorio y todo el contenido
        if (is_dir($directorio)) {
                // cuando se abre el directorio se generan algunas condiciones
                if ($dh = opendir($directorio)) {
                        //contador para que se separe las fotos por fila
                        $n = 0;
                        // en un ciclo while se podra leer la informacion del directorio y asi obtener los archivos
                        while (($archivos_directorio = readdir($dh)) !== false) {
                                // Se abriran solo los archivos sin 1 o 2 puntos antes, asi no se consideran otra clase de archivos
                                if ($archivos_directorio != "." && $archivos_directorio != "..") {
        ?>
                                        <!--de esta manera se generaran las filas con las fotos-->
                                        <!-- tambien se ingresan las dimensiones y caracteristicas -->
                                        <td><img src=<?php echo $directorio . $archivos_directorio ?> width="350px" ; height="350px" border="7"></td>
        <?php
                                        // en este caso el contador aumentara a 1 cuando se visualice la foto
                                        $n = $n + 1;
                                        // cuando el contador llegue a 4 fotos 
                                        if ($n % 4 == 0) {
                                                // se efectua un salto a la siguiente fila, asi hay 4 columnas (fotos) x fila
                                                echo ('</tr>');
                                        }
                                }
                        }
                }
        }
        // se cierra el directorio
        closedir($dh);
        // se finaliza la tabla
        echo "</table>";
        ?>
</body>

</html>