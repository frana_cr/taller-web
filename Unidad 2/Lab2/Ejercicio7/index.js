//funcion para poder buscar vocales en una frase
function buscadorVocales() {
  // de esta manera mediante una ventana el user ingresara una frase
  let frase = prompt("Ingresa la frase:");
  //cada vocal tendra un contador
  contador_letraA = 0;
  contador_letraE = 0;
  contador_letraI = 0;
  contador_letraO = 0;
  contador_letraU = 0;

  // se valida que se ingrese solo un valor
  if (isNaN(frase) != true) {
    alert("ERROR \nIngresa caracteres");
  }
  else {
    // Se crea un ciclo for para poder recorrer la frase y contar cada una de las vocales
    for (let i = 0; i < frase.length; i++) {
      // se establece la condicion de que busque en la frase una A (sea mayuscula o miniscula)
      if (frase[i] == "A" || frase[i] == "a") {
        // si la encuentra el contador aumenta
        contador_letraA = contador_letraA + 1;
      }
      // se establece la condicion de que busque en la frase una E
      if (frase[i] == "E" || frase[i] == "e") {
        // si la encuentra el contador aumenta
        contador_letraE = contador_letraE + 1;
      }
      // se establece la condicion de que busque en la frase una I
      if (frase[i] == "I" || frase[i] == "i") {
        // si la encuentra el contador aumenta
        contador_letraI = contador_letraI + 1;
      }
      // se establece la condicion de que busque en la frase una O
      if (frase[i] == "O" || frase[i] == "o") {
        // si la encuentra el contador aumenta
        contador_letraO = contador_letraO + 1;
      }
      // se establece la condicion de que busque en la frase una U
      if (frase[i] == "U" || frase[i] == "u") {
        // si la encuentra el contador aumenta
        contador_letraU = contador_letraU + 1;
      }
    }
    // fuera del ciclo for existira un booleano (true o false)
    // en este caso el booleano del contador cuando este sea mayor a 1 significara que existe la vocal
    if (Boolean(contador_letraA >= 1) == true) {
      alert("Existe la vocal A");
    }
    // si no se cumple la conficion del booleano se imprime que la vocal no existira
    else {
      alert("NO Existe la vocal A");
    }

    // Booleano que verifica la existencia de E si es true
    if (Boolean(contador_letraE >= 1) == true) {
      alert("Existe la vocal E");
    }
    // es es false no existe
    else {
      alert("NO Existe la vocal E");
    }
    // Booleano que verifica la existencia de I si es true
    if (Boolean(contador_letraI >= 1) == true) {
      alert("Existe la vocal I");
    }
    // si es false, no existira la vocal I
    else {
      alert("NO Existe la vocal I");
    }
    // Booleano que verifica la existencia de O si es true
    if (Boolean(contador_letraO >= 1) == true) {
      alert("Existe la vocal O");
    }
    // si no existe el booleano sera false
    else {
      alert("NO Existe la vocal O");
    }
    // Booleano que verifica la existencia de U si es true
    if (Boolean(contador_letraU >= 1) == true) {
      alert("Existe la vocal U");
    }
    // si el booleano es false se muestra que no existe tal vocal
    else {
      alert("NO Existe la vocal U");
    }
  }
}
