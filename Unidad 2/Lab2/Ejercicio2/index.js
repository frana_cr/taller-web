// Nombre y funcionamiento de la funcion
function sumaValores() {
  // se piden que se ingresen los dos valores
  let valor1 = prompt("Ingresa un primer valor:");
  let valor2 = prompt("Ingresa un segundo valor:");
  // Se establecen ciertas verificaciones de los valores
  // En este caso se verifica que el valor 1 no tenga campos vacios
  if (valor1 == "" || valor1 == null) {
    alert("ERROR \nCampos vacios");
  }
  // En este caso se verifica que el valor 2 no tenga campos vacios
  else if (valor2 == "" || valor2 == null) {
    alert("ERROR \nCampos vacios");
  }
  // En este caso se verifica que solo se ingresen numeros al valor 1
  else if (isNaN(valor1)) {
    alert("ERROR \nIngresa número");
  }
  // En este caso se verifica que solo se ingresen numeros al valor 2
  else if (isNaN(valor2)) {
    alert("ERROR \nIngresa número");
  }
  // finalmente luego de todas esas verificaciones se lleva a cabo la operacion
  else {
    // se realizara la suma de los valores, se ocupa parseINT para devolver el valor entero
    let suma = parseInt(valor1) + parseInt(valor2);
    // se imprime los resultados de la adicion
    texto = " Valores y suma: " + valor1 + " + " + valor2 + "=" + suma;
    alert(
      " Valores: " + valor1 + " + " + valor2 + " " + "Resultado Suma = " + suma
    );
    document.getElementById("demo1").innerHTML = texto;
  }
}
