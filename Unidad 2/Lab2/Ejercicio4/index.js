//funcion que desarrolla la comparacion de valores
function comparacionValoresMayores() {
  // Se pide al usuario por medio de diferentes ventanas que se ingresen cada uno de los valores
  let valor1 = prompt("Ingresa valor 1:");
  let valor2 = prompt("Ingresa valor 2:");
  let valor3 = prompt("Ingrese valor 3:");

  //Se establece la condicion de que todos los valores sean enteros
  // condicion para valor 1
  if(isNaN(valor1)== true){
    alert("ERROR \nIngresa datos numéricos");
  }
  // condicion para valor2
  if(isNaN(valor2)== true){
    alert("ERROR \nIngresa datos numéricos");
  }
   // condicion para valor3
  if(isNaN(valor3)== true){
    alert("ERROR \nIngresa datos numéricos");
  }
  // se valida tambien que los valores no esten vacios
   // condicion para valor 1
  else if(valor1 == ""){
    alert("ERROR \nCampo vacios");
  }
   // condicion para valor 2
  else if(valor2 == ""){
    alert("ERROR \nCampo vacios");
  }
   // condicion para valor 3
  else if(valor3 == ""){
    alert("ERROR \nCampo vacios");
  }
  // si no pasa nada de estas validaciones se lleva a cabo la comparacion
  else{
    // con comparacion y la funcion math max se sabra que numero es mayor
    let comparacion = Math.max(valor1, valor2, valor3)
    // Se imprime los valores respectivos y cual es el numero mayor 
    texto = " Valores: " + valor1 + " - " + valor2 + " - " + valor3 + " -> " + "Numero mayor: " + comparacion;
    alert(" Valores: " + valor1 + " - " + valor2 + " - " + valor3 + " -> " + "Numero mayor: " + comparacion);
  }
  document.getElementById("demo1").innerHTML = texto;
}