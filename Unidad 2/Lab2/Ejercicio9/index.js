// funcion que permite el cambio de clp a dolares
function cambioDolares(){
    // se captara la variable del dinero (metodo get) y se le añade un nuevo nombre
    let dineroPesoDolar=ejercicio9.dinero.value;
    // validacion: cuando el dinero de dolar sea igual a vacio significa que hay un error
    if (dineroPesoDolar == ""){
        // imprime un mensaje de alerta
        alert("ERROR \nCampos vacios")
    }
    // cuando no se ingresen valores numericos tambien saldra un error
    else if(isNaN(dineroPesoDolar)){
        alert("ERROR \nIngresa número")
    }
    //finalmente luego de las validaciones el valor de dinero en CLP se convierte a dolar (multiplica x valor de conversion)
    else{
        document.getElementById('dolar').value=parseFloat(dineroPesoDolar)*0.00117247;
        alert("GRACIAS POR TU CONVERSIÓN");
    }
}

// funcion que realiza la conversion de dolar a clp
function cambioClp(){
    // se captara la variable del dinero (metodo get) y se le añade un nuevo nombre
    let dineroPesoCLP=ejercicio9.dinero1.value;
     // validacion: cuando el dinero de dolar sea igual a vacio significa que hay un error imprime alerta
    if (dineroPesoCLP == ""){
        alert("ERROR \nCampos vacios")
    }
    // cuando no se ingresen valores numericos tambien saldra un error impreso en una alerta
    else if(isNaN(dineroPesoCLP)){
        alert("ERROR \nIngresa número")
    }
     //finalmente luego de las validaciones el valor de dinero en USD se convierte a peso chileno (multiplica x valor de conversion)
    else{
        document.getElementById('clp').value=parseFloat(dineroPesoCLP)*852.435;
        alert("GRACIAS POR TU CONVERSIÓN");
    }
}
