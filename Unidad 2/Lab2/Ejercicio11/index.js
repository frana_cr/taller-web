// funcion encargada de cargar la imagen cada vez que se produce la interaccion
function cargarImagen () {
    // se carga la imagen con el arreglo de las fotos y la posicion que le corresponda (para avanzar y retroceder)
    document.getElementById("imagen").src = imagenes_album[contador_posicionImagen];
}

// se comienza el archivo js con una alerta de lo que se esta realizando 
alert("Bienvenido al proyecto de fotos!")
// Se define una variable que contendra un arreglo con cada una de las fotografias (5) y su ruta
    let imagenes_album = ["../fotosEj11/gato.jpg", "../fotosEj11/nirvana.jpg", 
    "../fotosEj11/perro.jpg", "../fotosEj11/rihanna.jpg", "../fotosEj11/sour.jpg" ];
    // existira un contador de la posicion de la imagen en 0 
    let contador_posicionImagen = 0;

    // funcion que permitira pasar la imagen
    function pasarFoto() {
        // cuando el contador de la imagen  sea mayor o igual al largo del arreglo menos el valor de 1
        if(contador_posicionImagen >= imagenes_album.length - 1) {
            // el contador sera igual a 0 (no avanza)
            contador_posicionImagen = 0;
        } 
        // pero cuando sucede lo contrario entonces el contador aumenta (porque se ha avanzado)
        else {
            contador_posicionImagen++;
        }
        // se llama a la funcion que carga la imagen
        cargarImagen();
    }
    // esta funcion es la encargada de retroceder
    function retrocederFoto() {
        // si el contador de la imagen es menor o igual a 0
        if(contador_posicionImagen <= 0) {
            // entonces el contador sera igual al largo de las imagenes menor 1 valor
            contador_posicionImagen = imagenes_album.length - 1;
        } 
        // de modo contrario el contador disminuira
        else {
            contador_posicionImagen--;
        }
        // se llama a la funcion que carga la imagen 
        cargarImagen();
    }
