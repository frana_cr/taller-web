// Funcion que lleva a cabo la verificacion que si el numero es divisible
function numeroDivisible() {
    // se pide al usuario que ingrese el valor para ver si es divisible
    let valor = prompt("Ingresa el valor:");

    // se valida que se ingrese solo un valor
    if(isNaN(valor)== true){
        alert("ERROR \nIngresa datos numéricos");
    }
    // se valida que se no se ingresen campos vacios
    else if(valor == ""){
        alert("ERROR \nCampo vacios");
    }
    // luego de validad todo se realiza la funcion que vea si es divisible
    else{
        // con el if podremos ver si el valor es divisible por dos si este da un resto 0  por lo tanto es divisible
        if (valor % 2 == 0) {
        // imprime la alerta de que si es divisible
            alert("El numero" + " " + valor + " " + " \nSI es divisible");
            texto = "El numero" + " " + valor + " " + " \nSI es divisible";
        }
        // si no se cumple la condicion del resto (if)
        else {
            // imprime la alerta de que NO es divisible
            alert("El numero" + " " + valor + " " + " \nNO es divisible");
            texto = "NO es divisible" + " " + valor;
        }
    }

    document.getElementById("demo1").innerHTML = texto;

}