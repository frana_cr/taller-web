// funcion que contara las letras A en una frase
function contadorA() {
    // se solicita una frase por una ventana al usuario
    let frase = prompt("Ingresa la frase:");
    // se inicia el contador en 0
    contador = 0; 
    // se valida que se ingrese solo un valor
    if(isNaN(frase) != true){
        alert("ERROR \nIngresa carácteres");
    }
    // si se valida que existen solo caractres se lleva a cabo la funcion 
    else{
        // se recorrera la frase con un ciclo for para poder ver capturar la posicion de la letra A
        for(let i=0; i < frase.length; i++){
        // si encuentra una letra A mayuscula o minuscula el contador aumentara
            if(frase[i] == "A" || frase[i] == "a"){
                // aumenta el contador
                contador ++;
            }
        }
    // finalmente luego del proceso se imprime el total de las letras A en la frase
    alert("El total de letras A es igual a: \n" + contador);
    texto = "La frase" + " " +  frase + " " + "tiene" + " " + contador + " " + "A";
    }

    document.getElementById("demo1").innerHTML = texto;

  }