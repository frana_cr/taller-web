// funcionq ue cuenta las vocales en una frase
function contadorVocales() {
    // se ingresa una frase mediante una ventana
    let frase = prompt("Ingresa la frase:");
    // contador de cada una de las vocales inicializado en 0
    contador_letraA = 0;
    contador_letraE = 0;
    contador_letraI = 0;
    contador_letraO = 0;
    contador_letraU = 0;

    // se valida que se ingrese solo un valor
    if (isNaN(frase) != true) {
        alert("ERROR \nIngresa caracteres");
    }
    else {
        // Se crea un ciclo for para poder recorrer la frase y contar cada una de las vocales
        for (let i = 0; i < frase.length; i++) {
          // se establece la condicion de que busque en la frase una A (sea mayuscula o miniscula)
          if (frase[i] == "A" || frase[i] == "a") {
            // si la encuentra el contador aumenta
            contador_letraA = contador_letraA + 1;
          }
          // se establece la condicion de que busque en la frase una E
          if (frase[i] == "E" || frase[i] == "e") {
            // si la encuentra el contador aumenta
            contador_letraE = contador_letraE + 1;
          }
          // se establece la condicion de que busque en la frase una I
          if (frase[i] == "I" || frase[i] == "i") {
            // si la encuentra el contador aumenta
            contador_letraI = contador_letraI + 1;
          }
          // se establece la condicion de que busque en la frase una O
          if (frase[i] == "O" || frase[i] == "o") {
            // si la encuentra el contador aumenta
            contador_letraO = contador_letraO + 1;
          }
          // se establece la condicion de que busque en la frase una U
          if (frase[i] == "U" || frase[i] == "u") {
            // si la encuentra el contador aumenta
            contador_letraU = contador_letraU + 1;
          }
        }
        // Luego de realizar la operacion se imprime la frase y cada una de las vocales en un ventana de alerta
        alert("La frase" + " " +  frase + " " + "tiene" + "\n" + contador_letraA + "A" + " " + contador_letraE + "E"  + " " + contador_letraI  + "I" 
        + " " + contador_letraO + "O" + " " + contador_letraU + "U");
        // Luego de realizar la operacion se imprime la frase y cada una de las vocales en un texto en la ventana
        texto = "La frase" + " " +  frase + " " + "tiene" + " " + contador_letraA + "A" + " "
        + contador_letraE + "E"  + " " + contador_letraI  + "I" + " " + contador_letraO + "O" + 
        " " + contador_letraU + "U";
    }
    document.getElementById("demo1").innerHTML = texto;
  }