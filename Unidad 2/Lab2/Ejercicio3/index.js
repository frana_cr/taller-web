// con esta funcion se comparan los valores
function comparacionValores() {
  // se pide que se ingresen dos valores desde una ventana
  let valor1 = prompt("Ingresa un valor:");
  let valor2 = prompt("Ingresa un otro valor:");
    
  // Se establecen ciertas verificaciones de los valores
  // En este caso se verifica que el valor 1 no tenga campos vacios
  if (valor1 == "" || valor1 == null) {
    alert("ERROR \nCampos vacios");
  }
  // En este caso se verifica que el valor 2 no tenga campos vacios
  else if (valor2 == "" || valor2 == null) {
    alert("ERROR \nCampos vacios");
  }
  // En este caso se verifica que solo se ingresen numeros al valor 1
  else if (isNaN(valor1)) {
    alert("ERROR \nIngresa número");
  }
  // En este caso se verifica que solo se ingresen numeros al valor 2
  else if (isNaN(valor2)) {
    alert("ERROR \nIngresa número");
  }
  // finalmente luego de las validaciones se realiza la comparacion
  else{
    // con la funcion Math.max se pueden comparar los valores
    let comparacion = Math.max(valor1, valor2)
    // se impren los resultados en alerta y mostrados en la pagina
    texto = " Valores: " + valor1 + " y " + valor2  + " " + "Numero mayor:" + comparacion;
    alert("Los numeros ingresados son: \n" + valor1 + " - " + valor2);
    alert("El valor mayor es: \n" + comparacion);
  }
    document.getElementById("demo1").innerHTML = texto;

  }