// Funcion donde se insertara el nombre del user
function insercionNombre() {
  // Con el let y prompt se puede ingresar el nombre del usuario con la ventana
  let nombre = prompt("Ingresa tu nombre:");
  // validacion para que no se ingresen campos vacios
  if (nombre == null || nombre == "") {
    // En caso que existan datos null se indicara una ventana con error
    texto = " ERROR \nCampos vacios";
    alert(" ERROR \nCampos vacios");
  }
  // se verifica el nombre solo contenga caracteres
  else if(isNaN(nombre) != true){
    texto = " ERROR \nIngresa carácteres";
    alert("ERROR \nIngresa carácteres");
  }
  // de modo contrario se ingresa algo imprimira el mensaje
  else {
    // imprime y muestra la alerta de hola con el nombre de user
    texto = " Hola " + nombre;
    alert(" Hola " + nombre);
  }
  document.getElementById("demo").innerHTML = texto;
}
