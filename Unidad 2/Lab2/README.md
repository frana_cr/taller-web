# Laboratorio 2 Unidad 2.

### Explicacion general.
En el siguiente laboratorio se desarrolla la construcción de distintas paginas web en las que se muestra una serie de implementaciones de tablas con distitnas utilidades. Es un total de 11 archivos en donde cada uno tiene una distinta funcionalidad.
Se utilizan imagenes, formularios y algunas funciones de JavaScrip junto con distintas herramientas de HTML y CSS.

### Desarrollo.
Para el correcto funcionamiento del sitio que se ha creado se generaron 11 carpeta en cada una de ellas existen 2 archivos index.js en donde se escriben el script y algunas funciones requeridas en Lenguaje de JavaScrip y otro llamado index.html el cual hace posible la visualizacion desde un navegador de cada una de las paginas. Tambien existen 2 carpetas extras con imagenes del ejercicio 10 y 11 los cuales son necesarios para su funcionalidad y tambien hay un archivo 1 CSS el encargando del estilo de todas ellas.

Los archivos de cada ejercicio y sus funcionalidades son:

- Ejercicio1: Se escribe un programa en donde se pide nombre del usuario con un prompt y escriba un texto que diga ”Hola nombreUsuario”, para realizar una correcta funcionalidad del programa se llevan a cabo algunas validaciones.

- Ejercicio2: En este ejercicio se realiza una suma entre dos numeros los cuales son entregados por el usaurio la suma y los valores son entregados en una ventana de alerta e impresos en pantalla.

- Ejercicio3: En este programa se solicitan dos numeros al usuario y se realiza una compracion de cual de ellos es mayor, para poder llevar a cabo este programa se utiliza la funcion Math.max().

- Ejercicio4 : Este ejercicio es bastante similar al anterior, se escribe un programa que pida 3 n ́umeros y escriba en la pantalla el mayor de los tres.

- Ejercicio5: En este caso se pedira un numero que el usuario ingrese y luego se dira si este es divisible o no por dos. 

- Ejercicio6: Aqui el usuario tendra que ingresar una frase y se tendra que mostrara cuantes veces aparece en ella la letra A.

- Ejercicio7: A continuacion se tiene que escribir un programa que pida una frase y escriba las vocales que aparecen (A, E, I, O, U).

- Ejercicio8: En esta situacion tambien se pide ingresar una frase pero a diferencia del ejercicio anterior se tiene que indicar la cantidad de veces en las cuales aparece cada vocal.

- Ejercicio9: Para la resolucion de este ejercicio se pide que se realice un formulario que permita la conversion de dolares a pesos chilenos y viceversa. 

- Ejercicio10: Ahora en este ejercicio se debe escribir un programa el cual debe simular que se lanza un dado, para poder llevar a cabo esta interaccion se mostrara un mensaje de alerta y una foto del numero (1 al 6), estos se generan con un random.

- Ejercicio11: FInalmente este ultimo ejercicio requiere que se muestre un proyector con 5 fotografias, para poder realizar el funcionamiento de esta se tiene que avanzar y retroceder en la imagenes (ciclo).

Cabe destacar que cada uno de estos archivos esta validado por una serie de condiciones necesarias para su funcionamiento.

### Prerequisitos

- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (Visual Studio Code)


### Ejecutando Pruebas
- Para ejecutar el desarrollo del laboratorio1 se tiene que acceder mediante el local host del usuario y desde su carpeta public_html en donde luego se ingresa a la carpeta llamada taller web, posteriormente se accede a unidad 2 y lab 1. Se desplegara la serie de los ejercicios y se tienen que ejecutar seleccionando la que sea de nombre index.php o directamente en el numero de la carpeta deseada.
Ruta donde estan los archivos.

https://gitlab.com/frana_cr/taller-web/-/tree/main/Unidad%202/Lab2

### Construido con:

- Ubuntu: Sistema operativo.
- HTML: Lenguaje de programación.
- JavaScrip: Lenguaje de programación.
- Visual Studio Code: Editor de código.

### Creación
- Francisca Castillo (Desarrollo de código y edición README)
